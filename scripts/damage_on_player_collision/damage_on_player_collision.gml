function damage_on_player_collision(damage) {
	instance_destroy();
	if (other.armor > 0) {
		other.armor -= damage;
		var _armor = getUIArmor(0);
		_armor.step = other.armor;
		shakeObject(o_ui_armor, 0);
		resetKillcount();
	}
}