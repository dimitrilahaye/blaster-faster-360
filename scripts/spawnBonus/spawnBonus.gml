function spawnBonus(bonusName) {
	var _center_x = room_width / 2;
	var _center_y = room_height / 2;
	var _direction = random_range(75, 135);
	var _distance = random_range(room_height * 1.1, room_height * 1.3);
	var _x = _center_x + lengthdir_x(_distance, _direction);
	var _y = _center_y + lengthdir_y(_distance, _direction);
	instance_create_layer(_x, _y, "Instances", bonusName);
}