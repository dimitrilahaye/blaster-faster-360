/// @description shake the object which contained the wobble logic
function shakeObject(object, index) {
	var _o = instance_find(object, index);
	_o.wobbling = true;
}