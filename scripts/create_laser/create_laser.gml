/// @description create the laser instance
function create_laser(position, laser_object, dir) {
	var _distance = sprite_get_width(sprite_index) / 2;
	var _angle = 60;
	var _newAngle;
	if (position == "right") {
		_newAngle = image_angle - _angle;
	} else if (position == "left") {
		_newAngle = image_angle + _angle;
	} else {
		_newAngle = image_angle;
	}
	var _x = x + lengthdir_x(_distance, _newAngle) + hspeed;
	var _y = y + lengthdir_y(_distance, _newAngle) + vspeed;
	var _laser = instance_create_layer(_x, _y, "Instances", laser_object);
	
	_laser.direction = (dir == undefined) ? image_angle : dir;
	_laser.image_angle = (dir == undefined) ? image_angle : dir;

	return _laser;
}