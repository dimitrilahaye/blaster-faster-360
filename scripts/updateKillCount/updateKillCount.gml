/// @description increase the kill count. If we reach the killcount step, we allow to spawn a bonus.
function updateKillCount() {
	var _killcount = getUIKillCount(0);
	_killcount.kcount++;
	shakeObject(o_ui_killcount, 0);
	if (_killcount.kcount % global.kcount_step == 0) {
		with (o_bonus_spawner) {
			canSpawn = true;
		}
	}
}