// bonus 2 lasers!
function fire_2_lasers() {
	var _leftLaser = create_laser("left", o_laser);
	_leftLaser.speed = 8;
	var _rightLaser = create_laser("right", o_laser);
	_rightLaser.speed = 8;
}