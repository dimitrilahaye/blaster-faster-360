/// @description reset to 0 the killcount when player ship takes a damage
function resetKillcount() {
	var _killcount = getUIKillCount(0);
	_killcount.kcount = 0;
	audio_play_sound(a_kc_failed, 5, false);
}