{
  "bboxMode": 0,
  "collisionKind": 0,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 28,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5d9cfc25-987e-4d4d-bfe7-bf68fccb8d86","path":"sprites/s_enemy_2/s_enemy_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5d9cfc25-987e-4d4d-bfe7-bf68fccb8d86","path":"sprites/s_enemy_2/s_enemy_2.yy",},"LayerId":{"name":"d612cbb2-d64b-477e-adbf-ff4e9827d0a2","path":"sprites/s_enemy_2/s_enemy_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"s_enemy_2","path":"sprites/s_enemy_2/s_enemy_2.yy",},"resourceVersion":"1.0","name":"5d9cfc25-987e-4d4d-bfe7-bf68fccb8d86","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"s_enemy_2","path":"sprites/s_enemy_2/s_enemy_2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e6876527-8b28-4672-ac15-dba8ab96b073","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5d9cfc25-987e-4d4d-bfe7-bf68fccb8d86","path":"sprites/s_enemy_2/s_enemy_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"s_enemy_2","path":"sprites/s_enemy_2/s_enemy_2.yy",},
    "resourceVersion": "1.3",
    "name": "s_enemy_2",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d612cbb2-d64b-477e-adbf-ff4e9827d0a2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Ships",
    "path": "folders/Sprites/Ships.yy",
  },
  "resourceVersion": "1.0",
  "name": "s_enemy_2",
  "tags": [],
  "resourceType": "GMSprite",
}