/// @description add enemies if zero left
var _enemy_number = instance_number(o_ship_enemy_parent);

if (_enemy_number == 0) {
	var _spawnerEnemy1 = 3 + score div 10;
	var _spawnerEnemy2 = 1 + score div 20;
	spawnEnemies(_spawnerEnemy1, o_enemy_ship_1);
	spawnEnemies(_spawnerEnemy2, o_enemy_ship_2);
}