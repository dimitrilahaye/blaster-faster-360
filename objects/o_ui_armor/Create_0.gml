wobbling = false;
wobble = 0;
wobble_length = 30; // number of cycles to wobble the instance with
                   // only whole numbers are valid - ie: 1, 2, etc
wobble_speed = 5; // valid value range: 0 - 13 (decimals are ok)
wobble_intensity = 20; // the distance multiplier (decimals are ok)

// list for each armor level
// map(max_step, sprite_name)
available_armors = ds_map_create();
ds_map_add(available_armors, 4, s_armor_4);
ds_map_add(available_armors, 5, s_armor_5);
ds_map_add(available_armors, 6, s_armor_6);
ds_map_add(available_armors, 7, s_armor_7);
ds_map_add(available_armors, 8, s_armor_8);
ds_map_add(available_armors, 9, s_armor_9);
ds_map_add(available_armors, 10, s_armor_10);