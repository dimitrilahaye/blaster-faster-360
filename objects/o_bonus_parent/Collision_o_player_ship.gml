audio_play_sound(a_powerup, 3, false);
with (o_bonus_spawner) {
	displayTagline = true;
	canSpawn = false; // avoid double tag line issue
}
instance_destroy();