/// @description pick a random sprite + fall right to the ground

// list of all the bonus
bonusSpriteList = ds_list_create();
ds_list_add(
	bonusSpriteList,
	s_bonus_0,
	s_bonus_1,
	s_bonus_2,
	s_bonus_3,
	s_bonus_4,
	s_bonus_5,
	s_bonus_6,
	s_bonus_7,
	s_bonus_8,
);
// random sprite
var _index = random(ds_list_size(bonusSpriteList));
sprite_index = bonusSpriteList[| _index];

// fall down
direction = 270;
motion_add(direction, acceleration);
speed = 1;
image_angle = random(360);