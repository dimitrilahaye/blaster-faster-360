/// @description Throw life bonus according to step

// TODO perf issue, will call this method at each step
var _kcount = getUIKillCount(0);

var _bonus = instance_number(o_bonus_parent);
if (canSpawn &&
	_kcount.kcount >= global.kcount_step &&
	_kcount.kcount % global.kcount_step == 0
	&& _bonus == 0) {
	canSpawn = false;
	alarm[0] = 12;
}

if (displayTagline) {
	var _divider = 0.65 / room_speed;

	if (alpha <= 1) {
		alpha -= _divider;
	}

	xscale += _divider;
	yscale += _divider;

	if (alpha <= 0) {
		alpha = 1
		xscale = 4;
		yscale = 4;
		displayTagline = false;
	}
}