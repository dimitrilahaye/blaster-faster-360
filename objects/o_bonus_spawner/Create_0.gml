/// @description Set this object variables
alpha = 1
xscale = 4;
yscale = 4;
displayTagline = false;

// One day I guess this enum will be usefull
enum BonusEnum {
	life1,
	life2,
	life3,
	laser1,
	laser2,
	laser3,
	laser_starred,
	laser_crossed,
	laser_crossed_starred,
	exploding,
	armor_inscrease,
	armor_decrease,
}
// list of all the bonus
bonusList = ds_list_create();
ds_list_add(
	bonusList,
	o_bonus_life_1,
	o_bonus_life_2,
	o_bonus_life_3,
	o_bonus_weapon_1,
	o_bonus_weapon_2,
	o_bonus_weapon_3,
	o_bonus_weapon_starred,
	o_bonus_weapon_crossed,
	o_bonus_weapon_starred_crossed,
	o_bonus_exploding,
	o_bonus_armor_increase,
	o_bonus_armor_decrease,
);
// each bonus tag line
bonusTextList = ds_list_create();
ds_list_add(
	bonusTextList,
	"One life\nmore",
	"Two lives\nmore",
	"Three lives\nmore",
	"Mono laser\nloooooser!",
	"Double laser\nnoice!",
	"Triple laser\nokaaaaay!",
	"Starred laser\nOMG!",
	"Crossed laser\nexcellent!",
	"Starred crossed laser\nINSAAAANE!",
	"Oopsie",
	"One more\narmor slot",
	"One armor slot\nLEEESSSS!",
);