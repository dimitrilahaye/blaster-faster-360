/// @description Write tagline with Scribble
element
.align(fa_center, fa_middle)
.blend(c_white, alpha)
.transform(xscale, yscale, 0)
.draw(room_width div 2, room_height div 2);