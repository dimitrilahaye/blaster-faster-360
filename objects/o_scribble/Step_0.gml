/// @description Grow up and fade out
var _divider = 0.65 / room_speed;

if (alpha <= 1) {
	alpha -= _divider;
}

xscale += _divider;
yscale += _divider;

if (alpha <= 0) {
	instance_destroy();
}