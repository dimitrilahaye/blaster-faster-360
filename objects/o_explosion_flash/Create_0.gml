image_angle = random(360);
image_xscale = scale;
image_yscale = scale;
audio_play_sound(a_explode, 4, false);
var _offset = random_range(-8, 8);
var _numberOfExplosions = random_range(5, 20);
repeat(_numberOfExplosions) {
	instance_create_layer(x + _offset, y + _offset, "Effects", o_explosion_chunk);	
}
instance_create_layer(x, y, "ui", o_screenshake);
x = xprevious;
y = yprevious;