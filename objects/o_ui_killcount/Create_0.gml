// scribble configuration
scribble_font_set_default("f_score");
kcount_string = scribble("Killcount: " + string(kcount));

// wobbling configuration
wobbling = false;
wobble = 0;
wobble_length = 30; // number of cycles to wobble the instance with
                   // only whole numbers are valid - ie: 1, 2, etc
wobble_speed = 5; // valid value range: 0 - 13 (decimals are ok)
wobble_intensity = 20; // the distance multiplier (decimals are ok)