var _stringEffects = "";
var _string = "Killcount: " + string(kcount);
if (kcount >= global.kcount_step * 4) {
	_stringEffects+= "[rainbow]"
}
if (kcount >= global.kcount_step * 3) {
	_stringEffects+= "[pulse]"
}
if (kcount >= global.kcount_step * 2) {
	_stringEffects+= "[wave]"
}
if (kcount >= global.kcount_step) {
	_stringEffects+= "[jitter]"
}
kcount_string = scribble(_stringEffects + _string).wrap(900);
if (kcount >= global.kcount_step * 2) {
	kcount_string
		.typewriter_in(1, 60)
		.typewriter_ease(SCRIBBLE_EASE.ELASTIC, 0, -25, 1, 1, 0, 0.1);	
}	
kcount_string.draw(x + sprite_width - 3, y + 4);