/// @description Insert description here
// You can write your code in this editor
/// Draw wobble

var x_offset;

// has wobbling been initiated?
if wobbling {
    // increase wobble each step
    wobble += wobble_speed;
    }

// has wobbling exceeded the max number of cycles?
if wobble > round(wobble_length) * (pi*2) {
    // stop instance from wobbling
    wobbling = false;
    // reset wobble to zero again
    wobble = 0;
    }

// calculate wobble with a sine wave (0 to 1)
// & then multiply it by its intensity - this
// "wobble_intensity" dictates how many extra
// pixels it will wobble on a horizontal axis
x_offset = sin(wobble) * wobble_intensity;

// draw sprite with the x_offset value added relatively
//draw_sprite(sprite_index, image_index, x + x_offset, y);
// draw_text(x + sprite_width - 3 + x_offset, y + 4, "Killcount: " + string(kcount));
kcount_string = scribble("Killcount: " + string(kcount));
kcount_string
.wrap(900)
.typewriter_in(1, 60)
.typewriter_ease(SCRIBBLE_EASE.ELASTIC, 0, -25, 1, 1, 0, 0.1)
.draw(x + sprite_width - 3 + x_offset, y + 4);