display_set_gui_size(640, 360);
draw_set_font(f_score);
if (not audio_is_playing(a_music)) {
	audio_play_sound(a_music, 5, true);
}
draw_set_halign(fa_right);
var _score_x = room_width - 8;
var _score_y = 8;
draw_text(_score_x, _score_y, "Highscore: " + string(global.highscore));
draw_set_halign(fa_left);