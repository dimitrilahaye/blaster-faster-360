global.kcount_step = 2;

display_set_gui_size(640, 360);
draw_set_font(f_score);

score = 0;

if (not audio_is_playing(a_music)) {
	audio_play_sound(a_music, 5, true);
}

// draw armor
instance_create_layer(8, 8, "ui", o_ui_armor);
instance_create_layer(8, 8, "ui", o_bonus_spawner);

// draw kill_count
var _armor_height = sprite_get_height(s_armor_4);
var _kill_count_y = _armor_height + 8;
instance_create_layer(8, _kill_count_y + 3, "ui", o_ui_killcount);

// draw score
var _score_sprite_width = sprite_get_width(s_score);
var _score_x = room_width - 8 - _score_sprite_width;
var _score_y = 8;
instance_create_layer(_score_x, _score_y, "ui", o_ui_score);