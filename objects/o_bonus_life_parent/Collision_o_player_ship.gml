event_inherited();
var _armor = getUIArmor(0);
if (other.armor == other.max_armor) exit;
var _bonus = other.armor + life_amount;
if (_bonus > other.max_armor) _bonus = other.max_armor;
_armor.step = _bonus;
other.armor = _bonus;