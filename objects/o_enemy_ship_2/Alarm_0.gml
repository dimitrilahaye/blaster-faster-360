var _player_ship = getPlayer(0);
if (_player_ship == noone) exit;

alarm[0] = fire_rate;

if (not point_inside_room()) exit;

var _distance = point_distance(x, y, _player_ship.x, _player_ship.y);

if (_distance <= attack_range) {
	var _laser = create_laser("", o_laser_enemy);
	_laser.speed = 2;
}