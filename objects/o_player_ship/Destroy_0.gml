event_inherited();
instance_create_layer(x, y, "ui", o_death_timer);
if (global.highscore < score) {
	global.highscore = score;
	ini_open("save.ini");
	ini_write_real("Scores", "Highscore", global.highscore);
	ini_close();
}