var _right = keyboard_check(vk_right), _left = keyboard_check(vk_left), _up = keyboard_check(vk_up), _down = keyboard_check(vk_down);
var _fly = _right || _left || _up || _down;
image_index = _fly;
if (!_fly) {
	friction = friction_amount;
} else {
	if (_right) {
		if (_up) {
			direction = 75;
			image_angle = 75;
		} else if (_down) {
			direction = 315;
			image_angle = 315;
		} else {
			direction = 0;
			image_angle = 0;			
		}
	} else if (_left) {
		if (_up) {
			direction = 135;
			image_angle = 135;
		} else if (_down) {
			direction = 225;
			image_angle = 225;
		} else {
			direction = 180;
			image_angle = 180;			
		}
	} else if (_up) {
		if (_left) {
			direction = 135;
			image_angle = 135;
		} else if(_right) {
			direction = 75;
			image_angle = 75;
		} else {
			direction = 90;
			image_angle = 90;			
		}
	} else if (_down) {
		if (_left) {
			direction = 225;
			image_angle = 225;
		} else if(_right) {
			direction = 315;
			image_angle = 315;
		} else {
			direction = 270;
			image_angle = 270;			
		}
	}
	
	motion_add(image_angle, acceleration);
	if (speed > max_speed) {
		speed = max_speed;
	}
	var _numberOfParticles = random_range(2, 5);
	repeat(_numberOfParticles) {
		var _offset = random_range(-4, 4);
		var _length = -14;
		var _x = x + lengthdir_x(_length, image_angle) + _offset;
		var _y = y + lengthdir_y(_length, image_angle) + _offset;
		instance_create_layer(_x, _y, "Effects", o_explosion_particle);
	}
}

var _fire_laser = keyboard_check_pressed(vk_space);
if (_fire_laser) {
	regular_weapon();
}