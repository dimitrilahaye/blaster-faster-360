// Inherit the parent event
event_inherited();
// increase player's ship max armor
// and increase armor max step
if (other.max_armor > 4) {
	other.max_armor -= 1;
	with (o_ui_armor) {
		max_step -= 1;
	}
}